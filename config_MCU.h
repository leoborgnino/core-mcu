/********************************
 *        Constantes            *
/********************************/

#define INTE0                  2
#define INTE1                  3
#define MAIN_IMU_ADDR          0x68
#define SEC_IMU_ADDR           0x69
#define BAUD_RATE              115200 
#define CONTROL_PERIOD         20
#define TIME_SAMPLE            250 // FIX ME
#define TIME_SAMPLE2           200
#define DELTA_T                (double(TIME_SAMPLE) * CONTROL_PERIOD)/(1000.0)
#define MICRO_ADDR             10
#define PPV                    36.0
#define LONG_ARC               2.0 * 3.14 * 10.0 //longitud de arco de la rueda en cm
#define DIST_PULS              LONG_ARC / PPV
#define STEER_THRESHOLD        3.0
#define LIMITE_DESVIO          2
#define PWM1                   10//Motor Principal
#define PWM2                   9//Motor Volante
#define LIMITE_REVERSA         20
#define LIMITE_GIRO            2
#define ROTACION_VOLANTE       52.0 
#define PULSOS_VOLANTE         4
#define PASO_VOLANTE           ROTACION_VOLANTE / PULSOS_VOLANTE
#define GIRO_MOV               PASO_VOLANTE * 4.0
#define GIRO_LIMITE            PASO_VOLANTE * 4.0
#define GIRO_LEVE              PASO_VOLANTE * 3.0
#define GIRO_MAX               ROTACION_VOLANTE/2
#define GIRO_MIN               5.0
#define GIRO_MIN_ITER          10.0
#define RUIDO_ROTACION         0.05
#define REVERSA_MIN            100.0
#define REVERSA_MAX            30.0
#define ULTR_LIMITE            40.0
#define N_ULTR_SENSOR             3
#define ULTR_PERIOD               1
#define ULTRA_TRIGER             A1
#define ULTRA_ECHO               A3
#define ULTRB_TRIGER             A4
#define ULTRB_ECHO               17
#define ULTRC_TRIGER             A2
#define ULTRC_ECHO               16
#define MODO_ULTRASONIDO          1
#define MUESTRAS_DETECCION        3
#define LIMITE_MUESTRAS           2
#define PIN_ALARM                A8
#define ALARM_PERIOD             15
#define N_BEEPS                   1
#define SATURACION_INTEGRADOR  100.0


#define FIFO_DEPTH  12 // Profundidad FIFO
#define N_MOVS       6 // Numero de comandos a almacenar

#ifndef STATUS_MAIN_STRUCT
#define STATUS_MAIN_STRUCT
struct status_main_fsm
{
  // Variables de Movimiento
  bool flag_mov_rec;
  bool flag_mov_req;
  bool flag_stop_moving;
  bool flag_stop_moving_hard;
  int command[N_MOVS];    
    
  // Flags eventos microcontrolador
  bool flag_timer1; // 300us  Timer
  bool flag_timer2; // 1s     Timer
  bool flag_timer3; // Future Timer

  bool flag_ext_int0; // External Interupt 2
  bool flag_ext_int1; // External Interupt 3
  bool flag_ext_int2; // Future External Interupt

};
#endif
