#ifndef vehiculo_autonomo_h
#define vehiculo_autonomo_h

#ifndef ARDUINO
#include <iostream>
#else
#include <I2C.h>
#include <Wire.h>
#include <Arduino.h>
#endif

#include "config_MCU.h"

using namespace std;

#ifdef ARDUINO
extern status_main_fsm main_fsm_status;
#endif

// Main FSM
enum States {IDLE, WAIT_MOVE, MOVING, REFILL, STOP};

// Mov FSM
enum movStates {QUIET, FORWARD, ITERATION, DONE};

struct vehicle_state
{
  float velocidad_ref;
  float distancia_ref;
  float yaw_ref;

  float velocidad_act;
  float distancia_act;
  
  float pitch; // Y axis rotation
  float roll;  // X axis rotation
  float yaw;   // Z axis rotation

  float x_accel;
  float y_accel;
  float z_accel;

  float steer_x_accel;
  float steer_y_accel;
  float steer_z_accel;

  float steer_x_rot;
  float steer_y_rot;

  float steer_ref;
  
  bool forward;

  int fifo_level;
  // Move status
  bool flag_mov_en;
  bool flag_mov_stop;
  // States
  States state;
  movStates mov_state;
};

struct pid_controller_vars
{
  float distancia_temp_d;

  float ki,kp,kd;

  float p_controller;
  float i_controller;
  float d_controller;

  float prev_error;

  float pid_result;

  float saturation_value;
};

class vehiculo_autonomo
{

  // IMU0 Data
  float data[6];
  
  int imu0_address;
  float imu0_offset[3];

  int imu1_address;
  float imu1_offset[3];
  
  // FIFO Movimientos
  int wr_ptr, rd_ptr;
  int mov_matrix[FIFO_DEPTH][N_MOVS];
  bool fifo_full,fifo_empty;
  bool flag_seq_finished;

  // PID Struct
  pid_controller_vars traslation_pid_status;
  pid_controller_vars steering_pid_status;
  
  // Metodos privados  
  void         move_state_machine (       );
  unsigned int write_fifo         ( int * );
  unsigned int read_fifo          ( int * );
  void         interrupt_handler  (       );
  void         reset_vehiculo     (       );
  
  // Filtros PID
  float pid_controller ( pid_controller_vars *, float );
  void config_pid ( pid_controller_vars *, float, float, float, float );
  void stop_pid ( pid_controller_vars * );

  //
  float get_velocity_error();
  float get_steering_error();

  // Movimientos misc
  void steer_check();
  void steer_control();
    
  // Metodos de logueo de sensores
  void   imu_start_com( int );
  float* imu_get_all_data( int, float*, float* );
  void   calibrate_imu(int , float* );
  float  imu_get_z_gyro(int, float* );
    
  // Metodos y variables publicas
  public: 
    vehiculo_autonomo ();
    void main_state_machine    (  );
    // Variables de estado vehiculo
    vehicle_state  vehicle_status;
    status_main_fsm main_fsm_status;
};
#endif
