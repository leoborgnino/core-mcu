/**************************
 *   Start Comunication   *
 **************************/

void vehiculo_autonomo::imu_start_com(int address)
{
    
#ifdef ARDUINO
byte id;

  Wire.begin();
  Wire.beginTransmission(address);
  Wire.write(0x6B); // PWR_MGMT_1 registro a cero
  Wire.write(0);    // se saca el MPU del sleep
  Wire.endTransmission(true);
  Wire.beginTransmission(address);
  Wire.write(27); // Gyro config
  Wire.write(8);    // rango gyro
  Wire.endTransmission(true);
  
  I2c.begin ();  
  I2c.timeOut(100);  // milliseconds
  I2c.write (address, 0x75);

  if (I2c.read (address, sizeof id, &id) == 0)
    {
    Serial.print ("Slave is ID: ");
    Serial.println (id, DEC);
    Serial.print("Reset value: ");
    I2c.write (address, 0x6B);
    I2c.read (address, sizeof id, &id);
    Serial.println(id);
    }
  else
    Serial.println ("No response to ID request");              

#else
  cout << "IMU " << address << " Iniciado"  << endl;
#endif

}

float* vehiculo_autonomo::imu_get_all_data(int address, float *offset, float *data)
{  
  #ifdef ARDUINO
  float datos[7];
  
#ifdef WIRE_LIB
  Wire.beginTransmission(address);
  Wire.write(0x3B); // starting with register 0x3B (ACCEL_XOUT_H)
  Wire.endTransmission(false);
  Wire.requestFrom(address,14,true); // request a total of 14 registers
  
  for( int i = 0; i < 7; i++ )
    {
      if (i == 3)
      datos[6] = Wire.read()<<8|Wire.read();
      else if (i > 3)
  datos[i-1] = Wire.read()<<8|Wire.read();
      else
  datos[i] = Wire.read()<<8|Wire.read();
    }
      Wire.endTransmission(true);
#else
  byte buf[14];

  I2c.write(address,0x3B);
  if (I2c.read (address, sizeof buf, buf) == 0)
    {
      
      for( int i = 0; i < 7; i++ )
      {
        if (i == 3)
          datos[6] = buf[i*2] << 8 | buf[i*2+1];
        else if (i > 3)
          datos[i-1] = buf[i*2] << 8 | buf[i*2+1];
        else
          datos[i] = buf[i*2] << 8 | buf[i*2+1];
      }
      
    for( int i = 0; i < 3; i++)
      {
        if (datos[i] >= 0x8000) datos[i] = -((65535 - datos[i])+1);
        datos[i] = datos[i] / 16384.0;
        if (datos[i+3] >= 0x8000) datos[i+3] = -((65535 - datos[i+3])+1);
        datos[i + 3] = (datos[i + 3] / 131) - offset[i];            
      }

    data[0] = datos[0];
    data[1] = datos[1];
    data[2] = datos[2];
    data[3] = datos[3];
    data[4] = datos[4];
    data[5] = datos[5];
     
    }
  else
    Serial.println ("No response");
    

#endif

  
  #else
  cout << "Tomando todos los datos IMU address " << address << endl;
  #endif

}

float vehiculo_autonomo::imu_get_z_gyro(int address, float *offset)
{
  float z_gyro = 0;
  #ifdef ARDUINO  
  float read_data;  
  Wire.beginTransmission(address);
  Wire.write(0x47);                // starting with register 0x3B (ACCEL_XOUT_H
  Wire.endTransmission(false);
  Wire.requestFrom(address, 2, true); // request a total of 2 registers
  read_data = Wire.read() << 8 | Wire.read();
  if (read_data >= 0x8000)
    read_data = -((65535 - read_data) + 1);
  read_data = (read_data / 131) - offset[2];
  z_gyro = read_data;
  #else
  cout << "Tomando Z gyro IMU address " << address << endl << "Datos: " << z_gyro << endl;
  #endif

  return z_gyro;
}

void vehiculo_autonomo::calibrate_imu(int address, float *offset)
{

#ifdef ARDUINO
  float offsetX = 0,offsetY = 0, offsetZ = 0;
  float offsetaX = 0, offsetaY = 0, offsetaZ = 0;
  float temp;

  for (int i = 0; i < 3; i++)
    offset[i] = 0.;
  
  for (int i = 0; i < 100; i++)
    {
      Wire.beginTransmission(address);
      Wire.write(0x3B); // starting with register 0x3B (ACCEL_XOUT_H)
      Wire.endTransmission(false);
      Wire.requestFrom(address,14,true); // request a total of 14 registers
      offsetaX = Wire.read()<<8|Wire.read(); // 0x3B (ACCEL_XOUT_H) & 0x3C (ACCEL_XOUT_L)
      offsetaY = Wire.read()<<8|Wire.read(); // 0x45 (GYRO_YOUT_H) & 0x46 (GYRO_YOUT_L)
      offsetaZ = Wire.read()<<8|Wire.read(); // 0x47 (GYRO_ZOUT_H) & 0x48 (GYRO_ZOUT_L)
      temp =  Wire.read()<<8|Wire.read();
      offsetX = Wire.read()<<8|Wire.read(); // 0x3B (ACCEL_XOUT_H) & 0x3C (ACCEL_XOUT_L)
      offsetY = Wire.read()<<8|Wire.read(); // 0x45 (GYRO_YOUT_H) & 0x46 (GYRO_YOUT_L)
      offsetZ = Wire.read()<<8|Wire.read(); // 0x47 (GYRO_ZOUT_H) & 0x48 (GYRO_ZOUT_L)
      
      if (offsetX >= 0x8000) offsetX = -((65535 - offsetX)+1);
      offsetX = offsetX / 131;
      if (offsetY >= 0x8000) offsetY = -((65535 - offsetY)+1);
      offsetY = offsetY / 131;
      if (offsetZ >= 0x8000) offsetZ = -((65535 - offsetZ)+1);
      offsetZ = offsetZ / 131;
      
      offset[0] = offset[0] + offsetX;
      offset[1] = offset[1] + offsetY;
      offset[2] = offset[2] + offsetZ;
    }
  offset[0] = offset[0] / 100;
  offset[1] = offset[1] / 100;
  offset[2] = offset[2] / 100;
#else
  cout << "Calibrando IMU address " << address << endl;
#endif

}
