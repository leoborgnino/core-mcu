#include <Arduino.h>

extern vehiculo_autonomo* vehiculo;

void ISR_Timer3()
{
  vehiculo->main_fsm_status.flag_timer1 = 1;
}

void ISR_Timer()
{
  vehiculo->main_fsm_status.flag_timer2 = 1;
}

void ISR_INTE0() //PIN2 Motor Principal
{
  vehiculo->main_fsm_status.flag_ext_int0 = 1;
}

