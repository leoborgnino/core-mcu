#include "vehiculo_autonomo.h"
#include "config_MCU.h"

/*******************
 *   Constructor   *
 *******************/

vehiculo_autonomo::vehiculo_autonomo ()
{
  // Secuencia de reset
  reset_vehiculo();
  
  // Init physical actions

  //Main MPU6050 Address
  imu_start_com(MAIN_IMU_ADDR);
  calibrate_imu(MAIN_IMU_ADDR, imu0_offset);

  imu_start_com(SEC_IMU_ADDR);
  calibrate_imu(SEC_IMU_ADDR, imu1_offset);

  #ifndef ARDUINO
  cout << "Vehiculo instanciado" << endl;
  #else
  Serial.println("Vehiculo Instanciado");
  #endif
}

/* ************** */
/* Config Methods */
/* ************** */

void vehiculo_autonomo::config_pid(pid_controller_vars *pid, float kp, float ki,float kd, float saturation_value)
{
  pid->ki = ki;
  pid->kp = kp;
  pid->kd = kd;
  pid->p_controller = 0.;
  pid->i_controller = 0.;
  pid->d_controller = 0.;             
  pid->prev_error = 0.;
  pid->pid_result = 0.;
  pid->saturation_value = saturation_value;
  pid->distancia_temp_d = 0.;
}

void vehiculo_autonomo::stop_pid(pid_controller_vars *pid)
{
  pid->p_controller = 0.;
  pid->i_controller = 0.;
  pid->d_controller = 0.;             
  pid->prev_error = 0.;
  pid->pid_result = 0.;
  pid->distancia_temp_d = 0.;
}

void vehiculo_autonomo::reset_vehiculo()
{
// FIFO Mov Init
  wr_ptr = 0;
  rd_ptr = 0;
  vehicle_status.fifo_level = 0;
  fifo_full = 0;
  fifo_empty = 1;
  // FSM
  vehicle_status.state = IDLE;
  vehicle_status.mov_state = QUIET;
  // Default PID
  config_pid(&traslation_pid_status,0.226,1.09,0.000232,SATURACION_INTEGRADOR);
  config_pid(&steering_pid_status,0.5,0,0,25.0); // FIX ME: ver las ganancias
  // Init vehicle state
  vehicle_status.velocidad_ref      = 0.;
  vehicle_status.distancia_ref      = 0.;
  vehicle_status.yaw_ref            = 0.;  
  vehicle_status.velocidad_act      = 0.;
  vehicle_status.distancia_act      = 0.;
  vehicle_status.pitch              = 0.;
  vehicle_status.roll               = 0.;
  vehicle_status.yaw                = 0.;
  vehicle_status.steer_x_accel      = 0.;
  vehicle_status.steer_y_accel      = 0.;
  vehicle_status.steer_z_accel      = 0.;
  vehicle_status.steer_x_rot        = 0.;
  vehicle_status.steer_y_rot        = 0.;
  vehicle_status.steer_ref          = 0.;
  vehicle_status.forward            = true;
  vehicle_status.flag_mov_en        = false;
  vehicle_status.flag_mov_stop      = false;
  // Init main FSM
  main_fsm_status.flag_mov_rec           = false;
  main_fsm_status.flag_mov_req           = false;
  main_fsm_status.flag_stop_moving       = true;
  main_fsm_status.flag_stop_moving_hard  = false;
  main_fsm_status.flag_timer1            = false;
  main_fsm_status.flag_timer2            = false;
  main_fsm_status.flag_timer3            = false;
  main_fsm_status.flag_ext_int0          = false;
  main_fsm_status.flag_ext_int1          = false;
  main_fsm_status.flag_ext_int2          = false;
} 

/************************************
 *   Maquina de Estados Principal   *
 ************************************/

void vehiculo_autonomo::main_state_machine()
{
  int command_rd[N_MOVS];
    
  // Flags eventos microcontrolador
  interrupt_handler();

  // Acciones comunes
  

  // Acciones dependientes de estados
  
  switch (vehicle_status.state)
    {
    case IDLE:
    
      #ifndef ARDUINO
      cout << "State: IDLE" << endl;
      #endif

      // Movimiento
      if ( main_fsm_status.flag_mov_rec )
        {
	        wr_ptr = 0;
	        rd_ptr = 0;
	        vehicle_status.fifo_level = 0;
	        fifo_full = 0;
	        fifo_empty = 1;
          vehicle_status.state = WAIT_MOVE;
          write_fifo(main_fsm_status.command);
	        main_fsm_status.flag_stop_moving = true;
          main_fsm_status.flag_mov_req = true;
          main_fsm_status.flag_mov_rec = false;
          
      #ifndef ARDUINO
      cout << "State: WAIT_MOVE" << endl;
      #else
      Serial.println("Main State: WAIT MOVE");
      #endif
        }
      break;
    case WAIT_MOVE:

      if ( !main_fsm_status.flag_mov_req && main_fsm_status.flag_mov_rec)
	      main_fsm_status.flag_mov_req = true;
      
      // Movimiento
      if ( main_fsm_status.flag_mov_rec )
	    {
        main_fsm_status.flag_mov_rec = false;
	      write_fifo(main_fsm_status.command);
	      if ( fifo_full || (main_fsm_status.command[5] == 0 ) )
	      {
	        vehicle_status.flag_mov_en = 1;
	        main_fsm_status.flag_mov_req = false;
	        vehicle_status.state = MOVING;
          #ifndef ARDUINO
          cout << "State: MOVING" << endl;
          cout << "***********************" << endl;
          for (int ii = 0; ii < N_MOVS; ii++)
            cout << command_rd[ii] << endl;
          cout << "***********************" << endl;
          #else
          Serial.println("Main State: MOVING");
          #endif          
	      }
	    }
      break;
    case MOVING:

      // Movimiento
      if ( main_fsm_status.flag_stop_moving_hard )
        vehicle_status.state = STOP;
      if ( vehicle_status.mov_state == QUIET || vehicle_status.mov_state == DONE )
        {
          read_fifo(command_rd);          
          vehicle_status.yaw_ref = ( command_rd[0] ) ? -command_rd[1] : command_rd[1];
          vehicle_status.distancia_ref   = (command_rd[4]) ? vehicle_status.distancia_ref + command_rd[2] + command_rd[3]*256 : vehicle_status.distancia_ref - command_rd[2] - command_rd[3]*256;
          vehicle_status.velocidad_ref   = command_rd[5];
	        vehicle_status.flag_mov_en = 1;
          move_state_machine();
          if ( fifo_empty || (command_rd[5] == 0) )
          {
            vehicle_status.state = STOP;
          }
          else if (vehicle_status.fifo_level == FIFO_DEPTH/2)
          {
            vehicle_status.state = REFILL;                 
            #ifndef ARDUINO
              cout << "State: REFILL" << endl;
            #else
              Serial.println("State: REFILL");
            #endif
          }
        }
      else
        move_state_machine();
      break;
    case REFILL:
      if ( !main_fsm_status.flag_mov_req && main_fsm_status.flag_mov_rec)
	      main_fsm_status.flag_mov_req = true;

      // Movimiento
      if ( main_fsm_status.flag_stop_moving_hard )
        vehicle_status.state = STOP;
      if ( main_fsm_status.flag_mov_rec )
        write_fifo(main_fsm_status.command);

      if ( vehicle_status.mov_state == DONE )
       { 
          read_fifo(command_rd);
          vehicle_status.yaw_ref = ( command_rd[0] ) ? -command_rd[1] : command_rd[1];
          vehicle_status.distancia_ref   = (command_rd[4]) ? vehicle_status.distancia_ref + command_rd[2] + command_rd[3]*256 : vehicle_status.distancia_ref - command_rd[2] - command_rd[3]*256;
          vehicle_status.velocidad_ref   = command_rd[5];
	        vehicle_status.flag_mov_en = 1;
        }
      move_state_machine();

      if (fifo_full)
	{
	  vehicle_status.state = MOVING;
	  main_fsm_status.flag_mov_req = false;
	}
      else if (command_rd[5] == 0)
	vehicle_status.state = STOP;
      break;
    case STOP:

      #ifndef ARDUINO
      cout << "State: STOP" << endl;
      #else
      Serial.println("State: STOP");
      analogWrite(PWM1,127);
      stop_pid( &traslation_pid_status );
      #endif

      // Movimiento
      move_state_machine();
      reset_vehiculo();
      break;
    default:
      break;
    }
}

/************************************
 *  Maquina de Estados Movimientos  *
 ************************************/

void vehiculo_autonomo::move_state_machine()
{
  
  switch (vehicle_status.mov_state)
    {
    case QUIET:
      #ifndef ARDUINO
      cout << "Mov State: QUIET" << endl;
      #else
      Serial.println("Mov State: QUIET");
      #endif
      if (vehicle_status.flag_mov_en)
      {
	        vehicle_status.mov_state = FORWARD;
          #ifdef ARDUINO
          Serial.println("Mov State: FORWARD");
          #else
          cout << "Mov State: FORWARD" << endl;
          #endif
      }
      break;
    case FORWARD:

      if ( ((vehicle_status.distancia_act >= vehicle_status.distancia_ref && vehicle_status.forward) ||
	    (vehicle_status.distancia_act <= vehicle_status.distancia_ref && !vehicle_status.forward) ) &&
	  ( (vehicle_status.yaw+LIMITE_GIRO >= vehicle_status.yaw_ref) && (vehicle_status.yaw-LIMITE_GIRO <= vehicle_status.yaw_ref) ) )
	{
    #ifndef ARDUINO
      cout << "Mov State: DONE" << endl;
      cout << "Deteniendose " << pid_controller( &traslation_pid_status, get_velocity_error() ) << endl;
    #else
    Serial.println("Mov State: DONE");
    #endif
	  vehicle_status.mov_state = DONE;
	  vehicle_status.flag_mov_en = 0;
	}
      else if ((vehicle_status.distancia_act >= vehicle_status.distancia_ref && vehicle_status.forward ) ||
	       (vehicle_status.distancia_act <= vehicle_status.distancia_ref && !vehicle_status.forward) )
	{
	 #ifdef ARDUINO
   Serial.println("Mov State: ITERATION");
   #else
   cout << "Mov State: ITERATION" << endl;
   #endif
      vehicle_status.mov_state = ITERATION;
    vehicle_status.velocidad_ref = -vehicle_status.velocidad_ref;
    if (vehicle_status.forward)
	      vehicle_status.distancia_ref = vehicle_status.distancia_ref - REVERSA_MIN;
	  else
	    vehicle_status.distancia_ref = vehicle_status.distancia_ref + REVERSA_MIN;
    vehicle_status.forward = !vehicle_status.forward;
	}
      break;
    case ITERATION:
      if ((vehicle_status.distancia_act <= vehicle_status.distancia_ref && !vehicle_status.forward ) ||
	  (vehicle_status.distancia_act >= vehicle_status.distancia_ref && vehicle_status.forward) )
	{
  #ifdef ARDUINO
   Serial.println("Mov State: FORWARD");
   #else
   cout << "Mov State: FORWARD" << endl;
   #endif
	  vehicle_status.mov_state = FORWARD;
	  vehicle_status.velocidad_ref = -vehicle_status.velocidad_ref;
    if (vehicle_status.forward)
	    vehicle_status.distancia_ref = vehicle_status.distancia_ref - REVERSA_MIN;
	  else
	    vehicle_status.distancia_ref = vehicle_status.distancia_ref + REVERSA_MIN;
    vehicle_status.forward = !vehicle_status.forward;
	}
      break;
    case DONE:    
      if (vehicle_status.flag_mov_en == 1)
	vehicle_status.mov_state = FORWARD;
      break;

    default:
      break;
    }
}


/************************
 *   FIFO Movimientos   *
 ************************/

unsigned int vehiculo_autonomo::write_fifo( int *command )
{
  
  for (int ii=0; ii<N_MOVS; ii++)
    mov_matrix[wr_ptr][ii] = command[ii];

  //for (int ii=0; ii<N_MOVS; ii++)
  //  cout << mov_matrix[wr_ptr][ii] << endl;

  wr_ptr = (wr_ptr+1) % FIFO_DEPTH;
  vehicle_status.fifo_level++;
  if ( vehicle_status.fifo_level == FIFO_DEPTH )
    {
      #ifndef ARDUINO
      cout << "FIFO FULL" << endl;
      #else
      Serial.println("FIFO Full");
      #endif
      fifo_full = 1;
    }
  else
    {
      fifo_empty = 0;
      fifo_full = 0;
    }

  return fifo_full;
}

unsigned int vehiculo_autonomo::read_fifo( int *command )
{
  
  for (int ii=0; ii<N_MOVS; ii++)
    command[ii] = mov_matrix[rd_ptr][ii];
  
//  for (int ii=0; ii<N_MOVS; ii++)
//    cout << command[ii] << endl;
//    cout << mov_matrix[rd_ptr][ii] << endl;

  rd_ptr = (rd_ptr+1) % FIFO_DEPTH;
  
  vehicle_status.fifo_level--;
  if ( vehicle_status.fifo_level <= 0 )
    {
      #ifndef ARDUINO
      cout << "FIFO Empty" << endl;
      #else
      Serial.println("FIFO Empty");
      #endif
      fifo_empty = 1;
    }
  else
    {
      fifo_empty = 0;
      fifo_full  = 0;
    }

  return fifo_empty;
}

/* *********************************** */
/* Funciones de Control de movimientos */
/* *********************************** */

float vehiculo_autonomo::pid_controller(pid_controller_vars *pid, float error)
{
  float pid_contr = 0;

  pid->distancia_temp_d = vehicle_status.distancia_act;
  
  pid->p_controller  = pid->kp * error;
  pid->i_controller += pid->ki * error * DELTA_T;

  if (pid->i_controller >= SATURACION_INTEGRADOR)
    pid->i_controller = SATURACION_INTEGRADOR;

  pid->d_controller  = (pid->kd * (error - pid->prev_error)) / DELTA_T;
  pid->prev_error    = error;
  pid_contr          = pid->p_controller + pid->i_controller + pid->d_controller;
  pid->pid_result    = pid_contr;
  
  if( (pid->pid_result > 0) && (pid->pid_result > pid->saturation_value) )
      pid->pid_result = pid->saturation_value;
  else if ( (pid->pid_result < 0) && (pid->pid_result < -pid->saturation_value) )
      pid->pid_result = -pid->saturation_value;
   
  return pid->pid_result;
}


float vehiculo_autonomo::get_velocity_error()
{
  vehicle_status.velocidad_act = (vehicle_status.distancia_act - traslation_pid_status.distancia_temp_d)/(TIME_SAMPLE/1000.0); // FIX ME
  traslation_pid_status.distancia_temp_d = vehicle_status.distancia_act;
  return vehicle_status.velocidad_ref - vehicle_status.velocidad_act;
}

float vehiculo_autonomo::get_steering_error()
{
  return vehicle_status.yaw_ref - vehicle_status.yaw;
}


void vehiculo_autonomo::steer_control()
{
  float steer_error;
#ifdef ARDUINO
  vehicle_status.steer_x_rot = degrees(atan2(float(vehicle_status.steer_y_accel),sqrt((float(vehicle_status.steer_x_accel)*float(vehicle_status.steer_x_accel)) + (float(vehicle_status.steer_z_accel) * float(vehicle_status.steer_z_accel)))));
#endif
  steer_error = vehicle_status.steer_ref - vehicle_status.steer_x_rot;
//  Serial.print("Ref:");
//  Serial.println(vehicle_status.steer_ref);
//  Serial.print("Act:");
//  Serial.println(vehicle_status.steer_x_rot);
//  Serial.print("Error:");
//  Serial.println(steer_error);
  
#ifdef ARDUINO
  if ( abs(steer_error) > STEER_THRESHOLD )  
  {
    if (steer_error > 0)
      analogWrite(PWM2, 200 );
    else
      analogWrite(PWM2, 50 );    
  }
  else
    analogWrite(PWM2, 127 );
#endif
}

// Interrupt Handler

void vehiculo_autonomo::interrupt_handler() // Quien es?
{
  if ( main_fsm_status.flag_timer1 ) // 200ms  Timer
    {
      main_fsm_status.flag_timer1 = false;
    }

  if ( main_fsm_status.flag_timer2 ) // 25ms     Timer
    {
      imu_get_all_data(MAIN_IMU_ADDR, imu0_offset, data);

      vehicle_status.x_accel = data[0];
      vehicle_status.y_accel = data[1];
      vehicle_status.z_accel = data[2];
      vehicle_status.pitch   = data[3]; // Y axis rotation
      vehicle_status.roll    = data[4];  // X axis rotation
      vehicle_status.yaw     = vehicle_status.yaw + data[5] / ((1.0/TIME_SAMPLE)*1000) ;   // Z axis rotation    
      imu_get_all_data(SEC_IMU_ADDR, imu1_offset, data);
      vehicle_status.steer_x_accel = data[0];
      vehicle_status.steer_y_accel = data[1];
      vehicle_status.steer_z_accel = data[2];
      
      #ifdef ARDUINO
      unsigned int motor_ctrl; 
      float velocity_error = get_velocity_error();
      motor_ctrl = 127 - pid_controller( &traslation_pid_status, velocity_error);
      vehicle_status.forward = ( velocity_error >= 0 );
      //Serial.print("PID Traslation Control: ");Serial.println(motor_ctrl);
      //analogWrite(PWM1, motor_ctrl ); // FIXME Time
      if (vehicle_status.forward)
        analogWrite(PWM1, 20 );
      else
        analogWrite(PWM1, 230 );
      
      vehicle_status.steer_ref = ( vehicle_status.forward ) ? pid_controller( &steering_pid_status, get_steering_error()) : -pid_controller( &steering_pid_status, get_steering_error());
      steer_control(); // FIXME Time
      #else
      cout << "Avanzando " << pid_controller ( &traslation_pid_status, get_velocity_error() ) << endl;
      #endif
      main_fsm_status.flag_timer2 = false;
    }

  if ( main_fsm_status.flag_timer3 ) // Future Timer
    {
      main_fsm_status.flag_timer3 = false;
    }

  if ( main_fsm_status.flag_ext_int0 ) // External Interupt 2
    {
      if ( !vehicle_status.forward )
	      vehicle_status.distancia_act = vehicle_status.distancia_act - DIST_PULS;
      else
	      vehicle_status.distancia_act = vehicle_status.distancia_act + DIST_PULS;
      main_fsm_status.flag_ext_int0 = false;
    }

  if ( main_fsm_status.flag_ext_int1 ) // External Interupt 3
    {
      main_fsm_status.flag_ext_int1 = false;      
    }
  
  if ( main_fsm_status.flag_ext_int2 ) // Future External Interupt
    {
      main_fsm_status.flag_ext_int2 = false;      
    }
}

#include "imu_6_ejes.h"
