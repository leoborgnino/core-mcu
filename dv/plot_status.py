import matplotlib.pyplot as plt
import math

datos = open('core_mcu_status.log', 'r')

lines = datos.readlines()

splitted_data = []
for i in lines:
    data_replace = i.replace("\n", "")
    splitted_data.append(data_replace.split())
    
vel_ref = []
vel_act = []
distancia_ref = []
distancia_act = []
yaw_ref = []
yaw_act = []
vol_pos_ref = []
vol_pos_act = []
fifo_level  = []
main_fsm    = []
move_fsm    = []

PLOT_WINDOW = len(splitted_data)-1

for i in range(PLOT_WINDOW):
    vel_ref.append(splitted_data[i + 1][0])
    distancia_ref.append(float(splitted_data[i + 1][1]))
    yaw_ref.append(float(splitted_data[i + 1][2]))
    vel_act.append(float(splitted_data[i + 1][3]))
    distancia_act.append(float(splitted_data[i + 1][4]))
    yaw_act.append(float(splitted_data[i + 1][5]))
    vol_pos_ref.append(float(splitted_data[i + 1][6]))
    vol_pos_act.append(float(splitted_data[i + 1][7]))
    fifo_level.append(float(splitted_data[i + 1][8]))
    main_fsm.append(float(splitted_data[i + 1][9]))
    move_fsm.append(float(splitted_data[i + 1][10]))

x = []
for i in range(len(vel_ref)):
    x.append(i)

# Plot variables
f, (ax1, ax2, ax3, ax4, ax5) = plt.subplots(5, sharex=True, sharey=False)
ax1.set_title('Datos Vehiculo Autonomo')
ax1.plot(x, vel_ref, color='b')
ax1.plot(x, vel_act, color='r')
ax1.set_ylabel("Velocidad")
ax2.plot(distancia_ref, color='b')
ax2.plot(distancia_act, color='r')
ax2.set_ylabel("Distancia")
ax3.plot(x, yaw_act, color='b')
ax3.plot(x, yaw_ref, color='r')
ax3.set_ylabel("Yaw")
ax4.plot(x, vol_pos_ref, color='r')
ax4.plot(x, vol_pos_act, color='b')
ax4.set_ylabel("Volante")
ax5.plot(x, fifo_level, color='orange')
ax5.set_ylabel("FIFO Lev")
# f.subplots_adjust(hspace=0)
plt.setp([a.get_xticklabels() for a in f.axes[:-1]], visible=False)
plt.show()

# Plot Planning
x = []
y = []

x.append(distancia_act[4]*math.cos(yaw_act[4]*0.0174532925))
y.append(distancia_act[4]*math.sin(yaw_act[4]*0.0174532925))

for i in range(5,len(distancia_act)):
    x.append(distancia_act[i]*math.cos(yaw_act[i]*0.0174532925))
    y.append(distancia_act[i]*math.sin(yaw_act[i]*0.0174532925))
    
plt.plot(x,y,'r')
plt.show()

