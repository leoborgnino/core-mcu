#ifndef ARDUINO

#include "../vehiculo_autonomo.cpp"
#include "../config_MCU.h"
#include "dv_environment.h"
#include <iostream>
#include <fstream>
#include <string>
#include <stdlib.h>
#include <iomanip>
#include <sstream>
#include <vector>
#include <bitset>

int main()
{
  // Variables Lectura de Archivos
  string line[100];
  int cnt = 0;
  char buffer[256];
  ifstream commands_onboard_cpu ("../onboard-computer/commands.log");
  int movimientos_totales;

  // Variables Escritura de Archivos
  ofstream core_mcu_status("./dv/core_mcu_status.log");

  // Variable de Movimientos
  int movimientos_wr[N_MOVS];
  int movimientos_rd[N_MOVS];

  int commands_fifo[100][N_MOVS];
  
  // Instancia vehiculo autonomo
  vehiculo_autonomo car;

  // Instancias de interrupciones
  time_interrupt timer1_0_3s(0.3);
  time_interrupt timer2_1s(1.0);
  ext_interrupt ext_traslacion(DIST_PULS);

  float velocity_cast;

  bool start = true;
  
  core_mcu_status << setw(15) << "Velocidad Ref" << setw(15) << "Distancia Ref" << setw(15) << "Yaw Ref" << setw(15) << "Velocidad Act" << setw(15) << "Distancia Act" << setw(15) << "Yaw Act" << setw(15) << "Vol Pos Ref" << setw(15) << "Vol Pos" << setw(15) << "FIFO Level"<< setw(15) << "Main FSM" << setw(15) << "Move FSM" << endl;

  // Lectura archivo de comandos
  /*
    Comandos de movimiento:
    [0] sentido yaw ref: 1 negativo 0 positivo
    [1] valor de la orientacion absoluta a conseguir
    [2] distancia ref
    [3] distancia ref >> 256
    [4] sentido distancia ref: 1: adelante 0: atras
    [5] velocidad ref en cm/seg
  */
  if (commands_onboard_cpu.is_open())
  {
    while ( getline (commands_onboard_cpu,line[cnt]) )
    {
      istringstream ss(line[cnt]);
      string token;
      for (int ii = 0; ii< N_MOVS+1; ii++)
	{
	  getline(ss,token,' ');
	  if (ii >= 1)
	    {
	      commands_fifo[cnt][ii-1] = stoi(token);
	      cout << commands_fifo[cnt][ii-1] << endl;
	    }
	}
      
      //cout << line[cnt] << '\n';
      //buffer[0] = line[cnt][4];
      //cout << atoi(buffer) << endl;
      cnt++;
    }
    cout << "Movimientos totales: " << cnt << endl;
    movimientos_totales = cnt;
    commands_onboard_cpu.close();
  }
  else cout << "Unable to open file"; 
  
  while ( start || !(car.vehicle_status.state == 0) )
    {
      if (start == true)
	{
	  for (int ii = 0; ii < N_MOVS; ii++)
	    car.main_fsm_status.command[ii] = commands_fifo[movimientos_totales-cnt][ii];
	  car.main_fsm_status.flag_mov_rec = true;
	  cnt--;
	  start = false;
	}
      else if ( car.main_fsm_status.flag_mov_req == true && (cnt > 0))
	{
	  for (int ii = 0; ii < N_MOVS; ii++)
	    car.main_fsm_status.command[ii] = commands_fifo[movimientos_totales-cnt][ii];
	  car.main_fsm_status.flag_mov_rec = true;
	  cnt--;
	  cout << "Movimiento leido: " << cnt << endl;
	}
      else
	car.main_fsm_status.flag_mov_rec = false;
      // Ext interrupt set vel
      if (car.vehicle_status.velocidad_ref != 0.0)
	{
	  ext_traslacion.set_velocity(car.vehicle_status.velocidad_ref);
	  ext_traslacion.enable = 1;
	}
      else
	ext_traslacion.enable = 0;
      
      // Interrupt ticks
      timer1_0_3s.interrupt_tick();
      timer2_1s.interrupt_tick();
      ext_traslacion.interrupt_tick();
      // Flag comunication
      car.main_fsm_status.flag_timer1   = timer1_0_3s.event;
      car.main_fsm_status.flag_timer2   = timer2_1s.event;
      car.main_fsm_status.flag_ext_int0 = ext_traslacion.event;
      // Log Status
      if (car.main_fsm_status.flag_timer2)
	{
	  velocity_cast = (car.vehicle_status.forward) ? car.vehicle_status.velocidad_ref : -car.vehicle_status.velocidad_ref;
	  core_mcu_status << fixed << setprecision(4) << setw(15) << velocity_cast << setw(15) << car.vehicle_status.distancia_ref << setw(15) <<  car.vehicle_status.yaw_ref << setw(15) << car.vehicle_status.velocidad_act << setw(15) << car.vehicle_status.distancia_act << setw(15) << car.vehicle_status.yaw << setw(15) << car.vehicle_status.fifo_level << setw(15) << car.vehicle_status.state << setw(15) << car.vehicle_status.mov_state << endl;
	}
      car.main_state_machine();
    }
  
  core_mcu_status.close();
  commands_onboard_cpu.close();
}

#endif
