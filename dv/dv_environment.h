#ifndef ARDUINO
#define N_EXT_INT   2
#define N_TIMER_INT 2
#define CPU_TICK    0.1 // seconds for cpu tick
#define TIME_LOG_SAMPLE 1 // time in seconds to log vehicle status


struct ext_interrupt
{
  float step_pulse; 
  int internal_counter;
  int limit;
  int enable;
  
  int event;

  ext_interrupt(float step) // paso en cm
  {
    step_pulse = step;
    limit = 0;
    internal_counter = 0;
    enable = 0;
    event = 0;
  }

  void interrupt_tick()
  {
    if (limit != 0 && enable == 1)
      internal_counter++;
    event = 0;
    if ( (internal_counter == limit) && (limit != 0) && enable == 1)
      {
	event = 1;
	internal_counter = 0;
      }
  }

  void set_velocity (float velocity) // velocidad en cm/seg
  {
    limit = int((step_pulse/velocity)/CPU_TICK);
    cout << "LIMIT:" << limit << endl;
  }
  
};

struct time_interrupt
{
  int internal_counter;
  int time_limit;
  
  int event;

  time_interrupt(float time) // Time in seconds
  {
    time_limit       = int(time/CPU_TICK);
    internal_counter = 0;
    event = 0;
  }

  void interrupt_tick ()
  {
    internal_counter++;
    event = 0;
    if (internal_counter == time_limit)
      {
	event = 1;
	internal_counter = 0;
      }
  }
};
#endif
