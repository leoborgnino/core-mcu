#include <Arduino.h>

int               flag_uart;

int               data_len_rx;
unsigned char     SerRx;
unsigned char     data_rec[10];

int               id_data_request;
int               id_planing;

extern vehiculo_autonomo* vehiculo;

// Functions
void map_to_vehicle();
void data_request(int id_request);
void send_mov_req_command();

/* Aligned with onboard-computer */
enum ID_Request
  {
    ACCEL = 0,
    GYRO  = 1,
    IMU   = 2,
    MOV   = 3,
    ULTR  = 4,
    GPS   = 5,
    FSM   = 6,
    ALL   = 7
  };

/*******************************************************************************************************************************************************
 * Arma la trama con el dato a enviar y lo envía por la UART. La trama contiene un encabezado, la longitud del dato high y low, el identificador del ***
 * objeto que requirió el dato, la dirección desde la cual se envía el dato, el DATO y un fin de trama *************************************************
/*******************************************************************************************************************************************************/
void send_uart(char* data_send, int respuestaid)
{
  char formlist[200];
  int data_len    = 0;
  char caracter   = 0;
  int header      = 0;
  int iheader     = 0;
  int PL          = 0;
  int mod_header  = 0;
  int mod_iheader = 0;
  int size_l      = 0;
  int size_h      = 0;

  header = 160;
  iheader = 64;
  while (caracter != '!')
  {
    caracter = data_send[data_len];
    data_len++;
  }
  data_len--;
  PL = 16;
  mod_header = header + PL + ((data_len & 0xf0000) >> 16);
  mod_iheader = iheader;
  size_l = data_len & 0xff;
  size_h = (data_len & 0xff00) >> 8;
  formlist[0] = mod_header;
  formlist[1] = size_h;
  formlist[2] = size_l;
  formlist[3] = respuestaid;
  formlist[4] = MICRO_ADDR;

  for ( int i = 5 ; i < 5 + data_len ; i++)
    formlist[i] = data_send[i - 5];
  formlist[5 + data_len] = mod_iheader;

  for (int i = 0 ; i < data_len + 6; i++)
    Serial1.write(formlist[i]);
}

void receive_uart()
{
  SerRx = Serial1.read();
  if ( ( flag_uart == 0 ) && ( (SerRx & 0xE0) == 160) ) //Encontramos el MIT
  {
    flag_uart++;
    data_len_rx = (SerRx & 0x0F);
  }
  else if ( flag_uart == 1 )
  {
    data_len_rx = data_len_rx + (SerRx << 8);
    flag_uart++;
  }
  else if ( flag_uart == 2 )
  {
    data_len_rx = data_len_rx + SerRx;
    flag_uart++;
  }
  else if ( ( SerRx == MICRO_ADDR ) && ( flag_uart == 3 ) )
  {
    flag_uart++;
  }
  else if ( flag_uart == 4 )
  {
    data_rec[0] = SerRx;
    flag_uart++;
  }
  else if ( (flag_uart > 4) && (flag_uart < 5 + data_len_rx) )
  {
    data_rec[flag_uart - 4] = SerRx;
    flag_uart++;
  }
  else if ( (SerRx == 64) && ( flag_uart == (5 + data_len_rx) ) ) //termina la recepción de la trama
  {
    flag_uart = 0;
    map_to_vehicle();
  }
  else flag_uart = 0;

}

void data_request ( int id_request )
{
  String data;
  char buff[200];
  float datos[7];
  int len;

  data = "";
  if ( id_request == ACCEL || id_request == ALL  || id_request == IMU)
    data = data + String(vehiculo->vehicle_status.x_accel) + " " + String(vehiculo->vehicle_status.y_accel) + " " + String(vehiculo->vehicle_status.z_accel) + " ";
  if ( id_request == GYRO || id_request == ALL || id_request == IMU )
    data = data + String(vehiculo->vehicle_status.pitch) + " " + String(vehiculo->vehicle_status.roll) + " " + String(vehiculo->vehicle_status.yaw) + " ";
  if ( id_request == MOV || id_request == ALL )
  { 
    data = data + String(vehiculo->vehicle_status.velocidad_act)     + " " + String(vehiculo->vehicle_status.velocidad_ref)  +
            " " + String(vehiculo->vehicle_status.distancia_act)     + " " + String(vehiculo->vehicle_status.distancia_ref)  +
            " " + String(vehiculo->vehicle_status.yaw)               + " " + String(vehiculo->vehicle_status.yaw_ref      )  +
            " " + String(vehiculo->vehicle_status.steer_x_accel)     + " " + String(vehiculo->vehicle_status.steer_y_accel ) + 
            " " + String(vehiculo->vehicle_status.steer_z_accel)     + " " + String(vehiculo->vehicle_status.steer_ref)  + " " ;
            //Serial.println("Data ALL REQUEST");
  }
  //if ( id_request == ULTR || id_request == ALL ) FIXME TODO Implement
  //if ( id_request == GPS || id_request == ALL ) FIXME TODO Implement
  //if ( id_request == FSM || id_request == ALL ) FIXME TODO Implement
  data = data + "!";
  len =  data.length();
  data.toCharArray(buff,len+1);
  
  //Serial.println(data);
  //Serial.println(buff);
  
  send_uart(buff, id_data_request);
}

void map_to_vehicle()
{
  // Map
  // 103 -> Data Request
  // 107 -> Movimiento
  // 108 -> Hard Stop

  // Data Request
  if ( data_rec[1] == char(103) )
    {      
      id_data_request = data_rec[0];
      data_request ( data_rec[2] );
    }

  // Movimiento
  if ( data_rec[1] == char(107) )
    {
      id_planing = data_rec[0];
      for (int ii = 0; ii < N_MOVS; ii++)
	      vehiculo->main_fsm_status.command[ii] = data_rec[ii+2];
      vehiculo->main_fsm_status.flag_mov_rec = true;
    }

  // Hard Stop
  if ( data_rec[1] == char(108) )
    vehiculo->main_fsm_status.flag_stop_moving_hard = true;
}

void send_mov_req_command()
{
  send_uart((char*)"0 !", id_planing);
}

