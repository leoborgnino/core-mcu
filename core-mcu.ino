#include <TimerThree.h>
#include <TimerOne.h>
#include <Wire.h>


#include "config_MCU.h"
#include "vehiculo_autonomo.h"
#include "UART_Com.h"
#include "core_Interrupts.h"

// Functions
void reset_status();

// Instances
vehiculo_autonomo* vehiculo;

/**                                                                                                                                                                  
 * @name setup
 * @brief Initial Setup. Configura puertos, interrupciones, inicia instancias.
 * @param None
 */
void setup()
{
  // Configuracion Timer 1
  Timer1.initialize(double(TIME_SAMPLE)*1000.0); // Dispara cada TIME_SAMPLE ms
  Timer1.attachInterrupt(ISR_Timer);      // Activa la interrupcion y la asocia a ISR_Timer

  // Configuracion Timer 3
  Timer3.initialize(double(TIME_SAMPLE2)*1000.0);
  Timer3.attachInterrupt(ISR_Timer3);

  // TRISX
  pinMode(ULTRA_TRIGER,OUTPUT);
  pinMode(ULTRB_TRIGER,OUTPUT);
  pinMode(ULTRC_TRIGER,OUTPUT);
  pinMode(PIN_ALARM,OUTPUT);
  digitalWrite(ULTRA_TRIGER,LOW);
  digitalWrite(ULTRB_TRIGER,LOW);
  digitalWrite(ULTRC_TRIGER,LOW);

  // Serial Comunication
  Serial1.begin(BAUD_RATE);               // Inicio la transmision serie principal
  Serial.begin(9600);                     // Inicio la transmision serie de debug

  // PWM Configuration
  TCCR2B = TCCR2B & 0b11111000 | 0x01;   // Establece frecuencia pwm pines 9,10 a 31K
  analogWrite(PWM1, 127);                // PWM1 50% duty
  analogWrite(PWM2, 127);                // PWM2 50% duty


  // Interrupciones externas (Pines 18,19,2,3)
  attachInterrupt(digitalPinToInterrupt(INTE0), ISR_INTE0,    CHANGE); // Interrupcion externa en pin 2 por cambio de nivel
}

/**                                                                                                                                                                  
 * @name loop
 * @brief Main Loop. Cuando recibe datos desde la CPU realiza acciones.
 * @param None
 */
void loop()
{
  // Instancia vehiculo autonomo
  vehiculo = new vehiculo_autonomo();
  //reset_status();

  while (true)
  {
    vehiculo->main_state_machine();

    // Receive method
    if (Serial1.available() > 0)
      receive_uart();
    // Response mov method
    if ( vehiculo->main_fsm_status.flag_mov_req )
      send_mov_req_command();
    
  }
}

void reset_status()
{
  // Variables de Movimiento
  vehiculo->main_fsm_status.flag_mov_rec          = false;
  vehiculo->main_fsm_status.flag_stop_moving      = false;
  vehiculo->main_fsm_status.flag_stop_moving_hard = false;
  
  // Flags eventos microcontrolador
  vehiculo->main_fsm_status.flag_timer1 = false; // 300us  Timer
  vehiculo->main_fsm_status.flag_timer2 = false; // 1s     Timer
  vehiculo->main_fsm_status.flag_timer3 = false; // Future Timer

  vehiculo->main_fsm_status.flag_ext_int0 = false; // External Interupt 2
  vehiculo->main_fsm_status.flag_ext_int1 = false; // External Interupt 3
  vehiculo->main_fsm_status.flag_ext_int2 = false; // Future External Interupt

}
