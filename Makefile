CC=gcc
CXX=g++
RM=rm -f

SRCS= ./dv/dv_environment.cpp
OBJS=$(subst .cpp,.o,$(SRCS))

all: clean arduino dv_environment sim

dv_environment:$(OBJS)
	$(CXX) -o dv_environment $(OBJS)

arduino:
	@amake -v mega2560 %f

dv_environment.o: dv_environment.cpp

sim:
	./dv_environment

clean:
	$(RM) $(OBJS)
